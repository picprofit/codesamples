<?php

$appDir = "_app";

require_once "../{$appDir}/config/common.php";

//check for cache
$cacheName = $config["cache"] . md5($_SERVER['REQUEST_URI']) . ".cache";
//clear cache if params received ?act=cc
if(isset($_GET["act"]) && $_GET["act"] == "cc") {
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url = $url["path"];
    $cacheName = $config["cache"] . md5($url) . ".cache";
    unlink($cacheName) or die("=(");
}
if(
    $config["cacheEnabled"] &&
    file_exists($cacheName) &&
    filesize($cacheName) > 1024 &&
    time() < strtotime($config["cacheTime"], filemtime($cacheName))
) {
    if($_SERVER['REQUEST_URI'] == "/sitemap.xml")
        header('Content-Type: text/xml; charset=utf-8');
    $data = file_get_contents($cacheName);
    echo $data;
    die();
}
if($config["debug"]) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}
else {
    ini_set('display_errors', 0);
    error_reporting(0);
}

require_once "../{$appDir}/config/routes.php";
require_once "../{$appDir}/config/db.php";
require_once "../{$appDir}/config/menus.php";
require_once "../{$appDir}/controller.php";
require_once "../{$appDir}/models/db.php";
require_once "../{$appDir}/models/helpers.php";


$db = new DB($config, $menus, $appDir);

$app = new controller($routes, $db, $menus, $config, $appDir);
$app->route($routes);
