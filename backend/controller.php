<?php

class controller
{

    private $routes;
    private $db;
    private $data;
    private $menus;
    private $config;
    private $appDir;

    function __construct($routes, $db, $menus, $config, $appDir)
    {
        $this->routes = $routes;
        $this->menus = $menus;
        $this->db = $db;
        $this->config = $config;
        $this->appDir = $appDir;
    }

    function route()
    {

        $url = $_SERVER['REQUEST_URI'];
        $url = ltrim($url, "/");

        if ($url == $this->config["main"] || $url == "index.php") {
            $this->redirect("/");
        }
        if ($url == "") {
            $url = $this->config["main"];
        } //main page

        $urlArray = explode('/', $url);
        $methodName = "_" . str_replace(Array(".", "-"), "_", $urlArray[0]);
        if (array_key_exists($urlArray[0], $this->routes) && method_exists($this, $methodName)) {
            if (!$this->check($url, $this->routes[$urlArray[0]])) {
                preg_match("~" . $this->routes[$urlArray[0]] . "~", $url, $repairedUrl);
                //try to repair invalid url
                if (!empty($repairedUrl)) {
                    $this->redirect("/" . $repairedUrl[0]);
                } else {
                    //if root url
                    if (
                        $urlArray[count($urlArray) - 1] == "" &&
                        count($urlArray) == count(explode("/", $this->routes[$urlArray[0]]))
                    ) {
                        $this->goCache($_SERVER['REQUEST_URI'], $urlArray, $methodName . "_ind");
                        die();
                    }
                    $this->_404();
                    die();
                }
            }

            $this->goCache($_SERVER['REQUEST_URI'], $urlArray, $methodName);
            die();
        }

        preg_match("~(.*?)\/~", $url, $repairedUrl); //слэш в конце?
        if (!empty($repairedUrl)) {
            $this->redirect("/" . $repairedUrl[1]);
        }

        $this->_404();
        die();
    }

    /**
     * function load content from cache OR creates content and save it to cache
     *
     * @param $url
     * @param $urlArray
     * @param $methodName
     */
    function goCache($url, $urlArray, $methodName)
    {
        $cacheName = $this->config["cache"] . md5($url) . ".cache";
        $cacheExists = file_exists($cacheName);

        if (!$this->config["cacheEnabled"] ||
            (
                !$cacheExists ||
                ($cacheExists &&
                    time() > strtotime($this->config["cacheTime"], filemtime($cacheName))
                )
            )
        ) { //create cache
            ob_start();
            call_user_func(Array($this, $methodName), $url, $urlArray);
            $data = ob_get_contents();
            ob_end_clean();
            // Don't forget to serialize() or json_encode() if the content is not a string!
            if (is_null(error_get_last()) && $this->config["cacheEnabled"]) { //ошибок не было и кэш включен
                file_put_contents($cacheName, $data, LOCK_EX);
            }
        } else //or load from cache
        {
            $data = file_get_contents($cacheName);
        }
        echo $data;
    }

    /**
     * redirects to url
     *
     * @param $url
     */
    function redirect($url)
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $url);
        exit();
    }

    /**
     * compare url with pattern
     *
     * @param $url
     * @param $pattern
     * @return int
     */
    function check($url, $pattern)
    {
        $arr = Array();

        return (preg_match("~" . $pattern . "$~", $url, $arr) == 1) ? true : false;
    }

    /**
     *
     *
     * @param $url
     */
    function _page($url, $urlArray)
    {
        $data_ok = true;
        $this->data["content"] = "";
        $this->data["title"] = "";
        $this->data["description"] = "";
        $this->data["keywords"] = "";
        $this->data["url"] = $url;

        $urlParams = explode("-", $urlArray[1]);
        //generate page content here//

        $this->data["title"] = "Page title";
        ob_start();
        require("views/" . $this->config["theme"] . "/tpl.php");
        $this->data["content"] = ob_get_clean();

        helpers::render($this->data, $this->menus, $this->config, $this->appDir);

        if ($data_ok) {
            helpers::render($this->data, $this->menus, $this->config, $this->appDir);
        } else {
            $this->_404();
            die();
        }
    }

    function _sitemap($url, $urlArray)
    {
        $this->data["title"] = "Sitemap, page " . (int)$urlArray[1];
        $this->data["description"] = "Sitemap";
        $this->data["keywords"] = "";
        $this->data["url"] = $url;
        $content = "<ul>";
        $links = Array();
        $tab = $urlArray[1] * $this->config["sitemap"]; //begin
        $tabEnd = ($urlArray[1] + 1) * $this->config["sitemap"]; //end
        $counter = 0;
        for ($i = 1; $i <= $this->config["nodnoks"]; $i++) {
            for ($j = $i; $j <= $this->config["nodnoks"]; $j++) {
                if ($counter < $tab) {
                    $counter++;
                    continue;
                }
                $counter++;
                if ($counter >= $tabEnd) {
                    break 2;
                }
            }
        }
        foreach ($links as $link) {
            $content .= "<li><a href=\"/{$link[0]}\">{$link[1]}</a></li>";
        }
        $content .= "</ul>";

        $this->data["content"] = $content;

        helpers::render($this->data, $this->menus, $this->config, $this->appDir);
    }

    function _404()
    {
        header("HTTP/1.0 404 Not Found");
        $this->data["content"] = $this->config["404"];
        $this->data["near"] = "";
        $this->data["title"] = "Page not found";
        $this->data["about"] = "";
        $this->data["description"] = "";
        $this->data["keywords"] = "";
        helpers::render($this->data, $this->menus, $this->config, $this->appDir);

        $cacheName = $this->config["cache"] . "!!404";
        file_put_contents($cacheName,
            $_SERVER["HTTP_REFERER"] . "\n" .
            $_SERVER['REQUEST_URI'] . "\n\n",
            FILE_APPEND);
    }
}