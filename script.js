/**
 * MODAL WINDOW
 */
(function () {
    var i,
        j,
        modalEl,
        modals = document.getElementsByClassName('modal'),
        showFormButtons = document.getElementsByClassName("show-modal"),
        closeModal = document.getElementsByClassName("close-modal"),
        body = document.getElementsByTagName("body")[0],
        bodyOverflow = body.style.overflow;

    // When the user clicks on the button with class "show-modal",
    // open the modal, default or defined in data-form attr
    for (i = 0, j = showFormButtons.length; i < j; i++) {
        showFormButtons[i].addEventListener('click', function () {
            if (this.hasAttribute("data-form")) {
                modalEl = document.getElementById(this.getAttribute("data-form"));
            } else {
                modalEl = document.getElementById('contact-modal');
            }
            if (modalEl !== null) {
                modalEl.style.display = "block";
                bodyOverflow = "hidden";
            }
        });
    }

    // When the user clicks on <span> (x), close the modal
    for (i = 0, j = closeModal.length; i < j; i++) {
        closeModal[i].addEventListener('click', function () {
            hideModals(modals);
        });
    }
    // or when the user clicks anywhere outside of the modal, close it
    window.addEventListener('click', function (event) {
        if (event.target === modalEl) {
            hideModals(modals);
        }
    });

    function hideModals(modals) {
        for (i = 0, j = modals.length; i < j; i++) {
            modals[i].style.display = "none";
        }
        bodyOverflow = "visible";
    }
})();


/**
 * Submit form with ajax
 * There could be few forms on page, form prefix generated with backend passes here as param
 * <form .. onsubmit="event.preventDefault(); sendForm({{ formPrefix }});"
 * @param formId
 */
function sendForm(formId) {
    var i,
        j,
        http,
        submitUrl = "empty.html", //just for test
        form,
        userName,
        otherInputs,
        params,
        formField,
        sendButton = document.getElementById("btn-send-contact" + formId),
        resultBlock = document.getElementById("resultMessage" + formId),
        jsonResult,
        res;

    var formFields = [
        "CRAFT_CSRF_TOKEN",
        "action",
        "phone",
        "fromName",
        "secondName",
        "fromEmail",
        "msg",
        "formName"
    ];
    var formFieldsToClear = [
        "phone",
        "fromName",
        "secondName",
        "fromEmail",
        "msg"
    ];

    var personalizedMessages = {
        "thankYou": "Bedankt voor uw bericht, %name%!",
        "error": "Sorry, %name%, some error occurred while sending. Try to call us or send us an e-mail"
    };

    var messages = {
        "thankYou": "Bedankt voor uw bericht!",
        "error": "Sorry, some error occurred while sending. Try to call us or send us an e-mail"
    };

    sendButton.disabled = true;
    form = document.getElementById("form" + formId);

    params = "";
    userName = "";
    //building parameters string
    for (i = 0, j = formFields.length; i < j; i++) {
        formField = form.querySelector("[name='" + formFields[i] + "']");
        if (formField) {
            params += "&" + formFields[i] + "=" + encodeURIComponent(formField.value);
            if (formFields[i] === "fromName") {
                userName = formField.value;
            } else if (formFields[i] === "secondName") {
                userName += " " + formField.value;
            }
        }
    }

    //adding additional parameters
    otherInputs = form.querySelectorAll("[name^='messageFields[']");
    if (otherInputs) {
        otherInputs.forEach(function (el) {
            params += "&" + el.getAttribute("name") + "=" + encodeURIComponent(el.value);
        });
    }
    params += "&source=" + window.location.href;
    params = params.substr(1);

    //sending form

    http = new XMLHttpRequest();
    http.open("POST", submitUrl, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.setRequestHeader("X-Requested-With", "XMLHttpRequest");

    //Send the proper header information along with the request
    http.onreadystatechange = function () {
        // if (http.readyState === 4 && http.status === 200) {
        if (true) { //disabled for this sample
            if(http.responseText.length > 0) {
                jsonResult = JSON.parse(http.responseText);
            } else {
                jsonResult = {};
            }
            res = "";
            for (i in jsonResult.error) {
                if (jsonResult.error.hasOwnProperty(i)) {
                    res = res + "<li>" + jsonResult.error[i] + "</li>";
                }
            }
            if (res.length === 0) {
                showResultSuccess(resultBlock);
                res = "";
                if (userName.length > 1) {
                    res = personalizedMessages.thankYou;
                    res = res.replace("%name%", userName);
                } else {
                    res = messages.thankYou;
                }

                //clear form fields
                for (i = 0, j = formFieldsToClear.length; i < j; i++) {
                    formField = form.querySelector("[name='" + formFieldsToClear[i] + "']");
                    if (formField !== null) {
                        formField.value = "";
                    }
                }
                if (otherInputs) {
                    otherInputs.forEach(function (el) {
                        el.value = "";
                    });
                }

            } else {
                res = "<ul>" + res + "</ul>";
                showResultError(resultBlock);
            }
            document.getElementById("resultMessage" + formId).innerHTML = res;
            setTimeout(function () {
                sendButton.disabled = false;
            }, 1500);
        } else if (http.readyState === 4 && http.status !== 200) { //error occurred
            res = "";
            if (userName.length > 1) {
                res = personalizedMessages.error;
                res = res.replace("%name%", userName);
            } else {
                res = messages.error;
            }
            resultBlock.innerHTML = res;
            showResultError(resultBlock);
            setTimeout(function () {
                sendButton.disabled = false;
            }, 1500);
        }
    };
    http.send(params);
}

function showResultSuccess(resultBlock) {
    resultBlock.classList.add('success');
    resultBlock.classList.remove('error');
}

function showResultError(resultBlock) {
    resultBlock.classList.add('error');
    resultBlock.classList.remove('success');
}

/**
 * Sticky menu on scroll
 */
window.addEventListener('scroll', function () {
    var navBar,
        navBarHeight,
        scrollPos;

    navBar = document.getElementById('topmenu');
    navBarHeight = navBar.offsetHeight;
    scrollPos = window.scrollY;

    if (scrollPos <= navBarHeight) {
        navBar.className = ('');
    } else {
        navBar.className = ('sticky');
    }
});

/**
 * Show/hide one form field based on selection
 */
(function () {
    var i,
        j,
        selectId,
        selectEl,
        selectedOption,
        hiddenFields = document.getElementsByClassName("hiddenField"),
        hiddenField;

    for (i = 0, j = hiddenFields.length; i < j; i++) {
        hiddenField = hiddenFields[i];
        selectId = hiddenField.getAttribute("data-select");
        selectedOption = hiddenField.getAttribute("data-option");
        selectEl = document.getElementById(selectId);
        (function (selectEl, selectedOption, hiddenField) {
            window.addEventListener("load", function () {
                toggleField(selectEl, selectedOption, hiddenField)
            });
            selectEl.addEventListener("change", function () {
                toggleField(selectEl, selectedOption, hiddenField);
            });
        })(selectEl, selectedOption, hiddenField);
    }
})();

/**
 * Function hides or shows element(@hiddenField) depends of selected option(@selectedOption) of select list (@selectEl)
 * @param selectEl
 * @param selectedOption
 * @param hiddenField
 */
function toggleField(selectEl, selectedOption, hiddenField) {
    if (selectEl.value === selectedOption) {
        hiddenField.classList.add("visible");
        hiddenField.getElementsByTagName("input")[0].required = true;
    } else {
        hiddenField.classList.remove("visible");
        hiddenField.getElementsByTagName("input")[0].required = false;
    }
}

/**
 * Toggle menu on mobile device
 */
(function () {
    var i,
        j,
        toggleMenu = document.getElementsByClassName("toggle-menu"),
        menu,
        resHeight,
        navHeight,
        contactHeight;

    for (i = 0, j = toggleMenu.length; i < j; i++) {
        toggleMenu[i].addEventListener("click", function () {
            menu = document.getElementById("nav-wrap");
            resHeight = 0;
            if (menu.style.height === "" || parseInt(menu.style.height) === 0) {
                this.classList.add("change");
                navHeight = document.getElementsByClassName("nav")[0].clientHeight;
                contactHeight = document.getElementsByClassName("sidebar_contact")[0].clientHeight;
                resHeight = navHeight + contactHeight;
            } else {
                this.classList.remove("change");
            }
            menu.style.height = resHeight + "px";
        });
    }
})();

/**
 * Live filter for news page
 */
(function(){
    var i,
        j,
        k,
        l,
        liveFilter = document.getElementsByClassName("live-filter"),
        filterBy,
        blocks;

    for (i = 0, j = liveFilter.length; i < j; i++) {
        liveFilter[i].addEventListener("click", function () {
            filterBy = this.getAttribute("data-filter");
            blocks = document.getElementsByClassName("bank_item");
            for (k = 0, l = blocks.length; k < l; k++) {
                if (blocks[k].className.indexOf(filterBy) !== -1) {
                    blocks[k].style.display = 'block';
                } else {
                    blocks[k].style.display = 'none';
                }
            }
        });
    }
})();
